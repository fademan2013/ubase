package ubase.proccessor

import javax.annotation.processing.Messager
import javax.tools.Diagnostic

/**
 * <i>
 *<br>
 * 作者：Created by ly1054 on 2020/3/19.
 *<br>
 *邮箱：2791014943@qq.com
 * </i>
 */
class Logger(private val msg: Messager) {

    fun info(string: String) = msg.printMessage(Diagnostic.Kind.NOTE, string)

    fun warn(string: String) = msg.printMessage(Diagnostic.Kind.WARNING, string)

    fun error(string: String) = msg.printMessage(Diagnostic.Kind.ERROR, string)
}