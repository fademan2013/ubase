package ubase.proccessor

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.asClassName
import com.squareup.kotlinpoet.asTypeName
import javax.lang.model.element.*

/**
 * <i>
 *<br>
 * 作者：Created by ly1054 on 2020/3/19.
 *<br>
 *邮箱：2791014943@qq.com
 * </i>
 */
class AnnInfo(private val element: TypeElement) {

    var logger: Logger? = null

    val className: ClassName = element.asClassName()

    private val viewClass: ClassName = ClassName("android.view", "View")

    val packageName: String = getPackageName().qualifiedName.toString()

    var layoutId = -1

    val clickMap = hashMapOf<Int, ExecutableElement>()

    private fun getPackageName(): PackageElement {
        return (if (element.kind == ElementKind.PACKAGE) element else element.enclosingElement) as PackageElement
    }

    private fun getClassName(element: Element): ClassName {
        return element.asType().asTypeName() as ClassName
    }

//    fun genConBuilder(): FunSpec {
//        return FunSpec.constructorBuilder().build()
//    }

    fun genActivityFun(): FunSpec {
        val builder = FunSpec.builder("bind")
            .addParameter("target", ClassName("android.app", "Activity"))
        if (layoutId != -1) {
            builder.addStatement("target.setContentView(%L)", layoutId)
        }
        clickMap.forEach { (id, element) ->
            when (element.parameters.size) {
                //没有参数
                0 -> builder.addStatement(
                    "(findViewById(%L) as View).setOnClickListener{target.%N()}",
                    id
                )
                1 -> {
                    if (getClassName(element.parameters[0]) != viewClass) {
                        logger?.error("element.simpleName function parameter error")
                    }
                    builder.addStatement(
                        "(findViewById(%L) as View).setOnClickListener{target.%N(it)}",
                        id,
                        element.simpleName
                    )
                }
                //多个参数错误
                else -> logger?.error("element.simpleName function parameter error")
            }
        }
        return builder.build()
    }

    fun genViewFun(): FunSpec {
        val builder = FunSpec.builder("bind")
            .addParameter("view", ClassName("android.view", "View"))
        clickMap.forEach { (id, element) ->
            when (element.parameters.size) {
                //没有参数
                0 -> builder.addStatement(
                    "(view.findViewById(%L) as View).setOnClickListener{target.%N()}",
                    id
                )
                1 -> {
                    if (getClassName(element.parameters[0]) != viewClass) {
                        logger?.error("element.simpleName function parameter error")
                    }
                    builder.addStatement(
                        "(view.findViewById(%L) as View).setOnClickListener{target.%N(it)}",
                        id,
                        element.simpleName
                    )
                }
                //多个参数错误
                else -> logger?.error("element.simpleName function parameter error")
            }
        }
        return builder.build()
    }

    fun genFragment(): FunSpec {
        val builder = FunSpec.builder("bind")
            .returns(ClassName("android.view", "View"))
            .addParameter("inflater", ClassName("android.view", "LayoutInflater"))
            .addParameter("container", ClassName("android.view", "View"))
        if (layoutId == -1) {
            builder.addStatement("return null")
            return builder.build()
        }
        builder.addStatement("val view = inflater.inflate(R.layout.fragment_second, container, false)")
        clickMap.forEach { (id, element) ->
            when (element.parameters.size) {
                //没有参数
                0 -> builder.addStatement(
                    "(view.findViewById(%L) as View).setOnClickListener{target.%N()}",
                    id
                )
                1 -> {
                    if (getClassName(element.parameters[0]) != viewClass) {
                        logger?.error("element.simpleName function parameter error")
                    }
                    builder.addStatement(
                        "(view.findViewById(%L) as View).setOnClickListener{target.%N(it)}",
                        id,
                        element.simpleName
                    )
                }
                //多个参数错误
                else -> logger?.error("element.simpleName function parameter error")
            }
        }
        builder.addStatement("return view")
        return builder.build()
    }


}