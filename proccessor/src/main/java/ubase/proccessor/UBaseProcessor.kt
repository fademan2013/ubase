package ubase.proccessor

import com.google.auto.service.AutoService
import com.ly1054.sample.annotation.Click
import com.ly1054.sample.annotation.Layout
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.TypeSpec
import java.io.File
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.annotation.processing.SupportedSourceVersion
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.tools.Diagnostic

/**
 *<br>
 * 作者：Created by ly1054 on 2020/3/16.
 *<br>
 *邮箱：2791014943@qq.com
 * </i>
 */
@AutoService(Processor::class)
@SupportedSourceVersion(SourceVersion.RELEASE_8)
class UBaseProcessor : AbstractProcessor() {

    private val suffix = "_UBASE"

    private val annotationMap = hashMapOf<String, AnnInfo>()

    private fun message(msg: String) {
        processingEnv.messager.printMessage(Diagnostic.Kind.NOTE, msg)
    }

    override fun process(
        annotations: MutableSet<out TypeElement>?,
        roundEnv: RoundEnvironment?
    ): Boolean {


        roundEnv?.getElementsAnnotatedWith(Layout::class.java)?.forEach {
            processLayout(it)
        }

        roundEnv?.getElementsAnnotatedWith(Click::class.java)?.forEach {
            processClick(it)
        }


        annotationMap.forEach { (_, info) ->
            processingEnv.messager.printMessage(Diagnostic.Kind.WARNING, "======================")
            val file = FileSpec.builder(info.packageName, info.className.simpleName + suffix)
                .addType(
                    TypeSpec.objectBuilder(info.className.simpleName + suffix)
                        .addFunction(info.genActivityFun())
                        .addFunction(info.genFragment())
                        .addFunction(info.genViewFun())
                        .build()
                ).build()
            file.writeFile()
        }
        return true
    }

    private fun FileSpec.writeFile() {
        //文件编译后位置
        val kaptKotlinGeneratedDir = processingEnv.options["kapt.kotlin.generated"]
        val outputFile = File(kaptKotlinGeneratedDir!!).apply {
            mkdirs()
        }
        processingEnv.messager.printMessage(Diagnostic.Kind.WARNING, "======================")
        processingEnv.messager.printMessage(Diagnostic.Kind.WARNING, outputFile.toPath().toString())
        writeTo(outputFile.toPath())
    }

    private fun processClick(element: Element?) {
        val executableElement = element as ExecutableElement
        val typeElement = element.enclosingElement as TypeElement
        val className = typeElement.qualifiedName.toString()
        var annInfo = annotationMap[className]
        if (annInfo == null) {
            annInfo = AnnInfo(typeElement)
            annotationMap[className] = annInfo
        }
        executableElement.getAnnotation(Click::class.java).values.forEach {
            annInfo.clickMap[it] = executableElement
        }
    }

    private fun processLayout(element: Element?) {
        val typeElement = element as TypeElement
        val className = typeElement.qualifiedName.toString()
        var annInfo = annotationMap[className]
        if (annInfo == null) {
            annInfo = AnnInfo(typeElement)
            annotationMap[className] = annInfo
        }
        annInfo.layoutId = typeElement.getAnnotation(Layout::class.java).value
    }

}