package com.ly1054.ubase

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * <i>
 *<br>
 * 作者：Created by ly1054 on 2020/3/17.
 *<br>
 *邮箱：2791014943@qq.com
 * </i>
 */
object UBase {


    private val SUFFIX = "_UBase"

    fun inject(target: Activity) {
        val targetClass = target::class.java
        val className = targetClass.name
        try {
            val bindingClass = targetClass.classLoader.loadClass(className + SUFFIX)
            val method = bindingClass.getDeclaredMethod("bind", Activity::class.java)
            method.invoke(bindingClass.newInstance(), target)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun inject(target: Any, inflater: LayoutInflater, container: ViewGroup?) {
        val targetClass = target::class.java
        val className = targetClass.name
        try {
            val bindingClass = targetClass.classLoader.loadClass(className  + SUFFIX)
            val method =
                bindingClass.getDeclaredMethod("bind", LayoutInflater::class.java, View::class.java)
            method.invoke(bindingClass.newInstance(), inflater, container)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun inject(target: Any, view: View) {
        val targetClass = target::class.java
        val className = targetClass.name
        try {
            val bindingClass = targetClass.classLoader.loadClass(className  + SUFFIX)
            val method = bindingClass.getDeclaredMethod("bind", View::class.java)
            method.invoke(bindingClass.newInstance(), view)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}