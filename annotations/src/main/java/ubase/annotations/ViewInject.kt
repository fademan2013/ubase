package com.ly1054.sample.annotation

/**
 *<br>
 * 作者：Created by ly1054 on 2020/3/16.
 *<br>
 *邮箱：2791014943@qq.com
 * </i>
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.BINARY)
annotation class Layout(val value: Int = -1)

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.BINARY)
annotation class Click(vararg val values: Int)


